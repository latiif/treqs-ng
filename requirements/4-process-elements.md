# TReqs Process


In this section we describe refined requirements of [Req-4: Process treqs files](treqs-system-requirements.md#req-4-process-treqs-files).


<treqs-element id="9a8a627687f111eb9d1ec4b301c00591" type="requirement">

### Req-4.1: Process plantuml models
TReqs shall create and update or link embedded PlantUML in a gen folder. 

>Example:

```
treqs process ./requirements/4-process-elements.md
```

This command shall process all PlantUML models in this documentation file.

The following code block should lead to the creation of a png file in the folder called `gen` with the name `Sample.png` as derived from the label in the first line. A copy of the processed file should be created in the `gen` folder that contains a line should be added after the code block, embedding the generated picture.

```
@startuml Sample
Alice -> Bob: test
@enduml
```

**Links to:**
<treqs-link type="hasParent" target="c153021087f011eb8a15c4b301c00591"> 

- [Req-4: Process treqs files](treqs-system-requirements.md#req-4-process-treqs-files)
</treqs-link>
</treqs-element>

<treqs-element id="9df5647c220d11eca690f018989356c1" type="requirement">

### Req-4.2: Preserve references to embedded PlantUML files
If a line already exists after a PlantUML block that references the (generated) image, it shall not be generated again. Effectively, this means that running treqs process on the output file of treqs process produces the same file content.

treqs process shall work as depicted in the following activity diagram:

```
@startuml process_plantuml
start                                                                                    
:Generate png files for each plantuml block;                                             
:Create in-line markdown reference to png file;                                          
if (in-line markdown reference exists) then (no)                                         
  :add in-line markdown reference  
after plantuml block;
endif
stop
@enduml 
```

<treqs-link type="relatesTo" target="9a8a627687f111eb9d1ec4b301c00591" />

</treqs-element>

<treqs-element id="c153f342220e11eca165f018989356c1" type="requirement">

### Req-4.3: Replace plantuml blocks that are not in code blocks

Treqs process shall replace a PlantUML block if it has not been placed in a multi-line code block.

If a PlantUML block is present within a markdown code block (surrounded by three back ticks), treqs process will replace the entire block with the embedded picture.

<treqs-link type="relatesTo" target="9a8a627687f111eb9d1ec4b301c00591" />
</treqs-element>

<treqs-element id="c7e7946a96eb11ebbfd9c4b301c00591" type="requirement">

### Req-4.4: PlantUML Licenses
Note that [plantuml is available under several licenses](https://plantuml.com/faq#ddbc9d04378ee462). Because of [Conv-6: MIT License](treqs-system-requirements.md#conv-6-mit-license), we use the MIT licensed version of PlantUML. 

<treqs-link type="hasParent" target="8793c8ca96eb11ebaca5c4b301c00591" />
<treqs-link type="relatesTo" target="9a8a627687f111eb9d1ec4b301c00591" />
</treqs-element>

<treqs-element id="b73c475c220d11ec9a73f018989356c1" type="requirement">


### Req-4.5: Allow for extensions in treqs process

TReqs shall allow to replace the inbuilt processing capabilities with external strategies.

To do so, treqs process checks whether the module `treqsext` is installed and contains the class `advanced_processor.advanced_processor`. If that is the case, the `process_elements` function of that class is used instead of the default.

**Links to:**
<treqs-link type="hasParent" target="c153021087f011eb8a15c4b301c00591"> 

- [Req-4: Process treqs files](treqs-system-requirements.md#req-4-process-treqs-files)
</treqs-link>
</treqs-element>