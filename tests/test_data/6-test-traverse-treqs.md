<treqs-element id="0276e84ac79011ebb719f018989356c1" type="requirement">

### 6-test-1 TReqs file traverser shall be able to handle documents without root tag.

TReqs file traverser uses an XML parser, but should also work with documents that do not have a root tag.
</treqs-element>

<p>
<treqs-element id="ff403b04c78f11ebbdc9f018989356c1" type="requirement">

### 6-test-2 TReqs file traverser shall be able to handle treqs-elements that are not under the root.

TReqs file traverser originally used only elements that were located directly under the root. However, it should be able to deal with them at any depth of the XML tree.
</treqs-element>
</p>

<treqs-element id="c5ae0c10c79211eb9631f018989356c1" type="requirement">

### 6-test-3 TReqs file traverser shall be able to handle treqs-elements in non-md files.

TReqs file traverser uses an XML parser. We modified it to process non-XML files, but we should explicitly verify this capability.
</treqs-element>