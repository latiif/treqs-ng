<treqs>

<treqs-element id="abeb5e8ce0b711ebaa057085c2221ca0" type="requirement">

### Recognize invalid inlinks
* First link should be correct
* Second treqs link should have invalid link type
* Third treqs link should

TReqs check should detect when inlinks are invalid based on TTIM file.

<treqs-link type="NoType" target="afca2be7e0b711eb9b8d7085c2221ca0" />
<treqs-link type="WrongType" target="afca2be7e0b711eb9b8d7085c2221ca0" />
<treqs-link type="hasParent" target="48773577e0c611eb85447085c2221ca0" />

</treqs-element>

<treqs-element id="ae05cc8de0b711eb83b37085c2221ca0" type="stakeholder-requirement">

### Recognize invalid outlinks

treqs check should recognize if outlinks are wrong based on TTIM file.
<treqs-link type="addresses" target="afca2be7e0b711eb9b8d7085c2221ca0" />
<treqs-link type="addresses" target="abeb5e8ce0b711ebaa057085c2221ca0" />

</treqs-element>

<treqs-element id="afca2be7e0b711eb9b8d7085c2221ca0" type="unittest">

### Recognize if link is required

treqs check should recognize if link is required based on TTIM file.
<treqs-link type="relatesTo" target="afca2be7e0b711eb9b8d7085c2221ca0" />
</treqs-element>

<treqs-element id="48773577e0c611eb85447085c2221ca0" type="stakeholder-requirement">

Description:
add additional information/details here
</treqs-element>

</treqs>
