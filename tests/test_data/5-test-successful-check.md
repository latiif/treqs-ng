<treqs-element id="a3ec00fc0bd811ecb4eef018989356c1" type="requirement">

### 5b-test-1 TReqs check shall exit successfully if there is no error.

TReqs check should exit with success if there is nothing wrong.
</treqs-element>

<treqs-element id="5313e0ce0c0411ec9f2bf018989356c1" type="unittest">

### 5b-test-2 TReqs check shall correctly validate required traces

If an element requires a trace link and has the expected link, it shall be correctly identified.
<treqs-link type="tests" target="a3ec00fc0bd811ecb4eef018989356c1" />
</treqs-element>

<treqs-element id="b8187a2e629311ee98f915f1b0e25002" type="unittest">

### 5b-test-3 TReqs check shall correctly validate links with multiple target types

If an element has link(s) that can target multiple types, it shall be correctly
identified and validated.
<treqs-link type="tests" target="a3ec00fc0bd811ecb4eef018989356c1" />
</treqs-element>
