import unittest
import logging
from treqs.file_traverser import file_traverser

class TestFileTraverser(unittest.TestCase):

    def setUp(self):
        self.logger = logging.getLogger('treqs-on-git.treqs-ng')

    # handler that prints the type and id of each element, then just returns successfully
    def print_handler_function(self, file_name, element=""):
        if (element != ""):
            self.logger.info("   | %s | %s |", element.get("type"), element.get("id"))
        return 0

    # handler that prints the tag of each element, then just returns successfully
    def print_tag_handler_function(self, file_name, element=""):
        if (element != ""):
            self.logger.info(element.tag)
        return 0

    # handler that just returns successfully
    def silent_handler_function(self, file_name, element=""):
        return 0

    # handler that just returns unsuccessfully
    def failing_handler_function(self, file_name, element=""):
        return 1

    def test_existing_file_traverser(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
                f_traverse = file_traverser()
                success = f_traverse.traverse_file_hierarchy('./tests/test_data/5-test-faulty-types-and-links.md', True, self.print_handler_function, f_traverse.traverse_XML_file, ".//treqs-element")

                self.assertEqual(success, 0)

        self.assertEqual(len(captured.records), 13)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "\n\nCalling XML traversal with filename ./tests/test_data/5-test-faulty-types-and-links.md")
        self.assertEqual(captured.records[2].getMessage(), "   ### Processing elements in File ./tests/test_data/5-test-faulty-types-and-links.md")
        self.assertEqual(captured.records[3].getMessage(), "   | non-existing-type | e770de36920911eb9355f018989356c1 |")
        self.assertEqual(captured.records[4].getMessage(), "   | requirement | 2c600896920a11ebbb6ff018989356c1 |")
        self.assertEqual(captured.records[5].getMessage(), "   | requirement | 56cbd2e0920a11ebb9d1f018989356c1 |")
        self.assertEqual(captured.records[6].getMessage(), "   | requirement | None |")
        self.assertEqual(captured.records[7].getMessage(), "   | requirement | 940f4d62920a11eba034f018989356c1 |")
        self.assertEqual(captured.records[8].getMessage(), "   | requirement | 940f4d62920a11eba034f018989356c1 |")
        self.assertEqual(captured.records[9].getMessage(), "   | requirement | c5402c1a919411eb8311f018989356c1 |")
        self.assertEqual(captured.records[10].getMessage(), "   | requirement | 4f5bcadad45711eb9de4f018989356c1 |")
        self.assertEqual(captured.records[11].getMessage(), "   | requirement | 3cabd686d45811ebaaeef018989356c1 |")
        self.assertEqual(captured.records[12].getMessage(), "   |  | 9d3a98c80bd711ec8e3ff018989356c1 |")

    def test_generic_traverser_strategy(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
                f_traverse = file_traverser()
                success = f_traverse.traverse_file_hierarchy('./tests/test_data/5-test-faulty-types-and-links.md', True, self.silent_handler_function)

                self.assertEqual(success, 0)

        self.assertEqual(len(captured.records), 3)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "Choosing generic traversal strategy")
        self.assertEqual(captured.records[2].getMessage(), "   ### Processing elements in File ./tests/test_data/5-test-faulty-types-and-links.md")

    def test_robustness_on_parser_errors(self):
        ''' Tests that no exception occurs when processing binary files. '''
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
                f_traverse = file_traverser()
                success = f_traverse.traverse_file_hierarchy('./tests/test_data/binary_file.png', True, self.silent_handler_function, f_traverse.traverse_XML_file)

                self.assertEqual(success, 0)
                self.assertEqual(len(captured.records), 4)
                self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
                self.assertEqual(captured.records[1].getMessage(), "\n\nCalling XML traversal with filename ./tests/test_data/binary_file.png")
                self.assertEqual(captured.records[2].getMessage(), "   ### Processing elements in File ./tests/test_data/binary_file.png")
                self.assertEqual(captured.records[3].getMessage(), "   ### Skipping elements in File ./tests/test_data/binary_file.png due to UnicodeDecodeError")

    def test_failing_handler_recursive(self):
        ''' Tests that the handler success variable is reached through to the traversal return value, and that no exception occurs. '''
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
                f_traverse = file_traverser()
                success = f_traverse.traverse_file_hierarchy('./tests/test-ignored-files/', True, self.failing_handler_function)

                self.assertEqual(success, 1)
                self.assertEqual(len(captured.records), 4)

                records = [r.getMessage() for r in captured.records[0:4]]
                self.assertIn("file_traverser created", records)
                self.assertIn("Choosing generic traversal strategy", records)
                self.assertIn("   ### Ignoring file tests/test-ignored-files/ignored-file.md (.treqs-ignore)", records)
                self.assertIn("   ### Processing elements in File tests/test-ignored-files/non-ignored-file.md", records)

    def test_failing_handler_non_recursive(self):
        ''' Tests that the handler success variable is reached through to the traversal return value, and that no exception occurs. '''
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
                f_traverse = file_traverser()
                success = f_traverse.traverse_file_hierarchy('./tests/test-ignored-files/', False, self.failing_handler_function)

                self.assertEqual(success, 1)
                self.assertEqual(len(captured.records), 5)
                self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
                self.assertEqual(captured.records[1].getMessage(), "Choosing generic traversal strategy")
                self.assertEqual(captured.records[2].getMessage(), "   ### Ignoring file tests/test-ignored-files/ignored-file.md (.treqs-ignore)")
                self.assertEqual(captured.records[3].getMessage(), "\n\n### Non-recursive file_traverser, skipping directory tests/test-ignored-files/ignored_subdir")
                self.assertEqual(captured.records[4].getMessage(), "   ### Processing elements in File tests/test-ignored-files/non-ignored-file.md")

    def test_non_existing_file_traverser(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            f_traverse = file_traverser()
            success = f_traverse.traverse_file_hierarchy('./tests/test_data/6-link.md', True, self.silent_handler_function, f_traverse.traverse_XML_file, ".//treqs-element")

        self.assertEqual(success, 1)

        self.assertEqual(len(captured.records), 2)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "\n\n### File or directory ./tests/test_data/6-link.md does not exist. Skipping.")

    def test_non_recursive_file_traverser(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            f_traverse = file_traverser()
            success = f_traverse.traverse_file_hierarchy('./tests/test_data/test-recursive-traverser', False, self.silent_handler_function, f_traverse.traverse_XML_file, ".//treqs-element")

            self.assertEqual(success, 0)
        self.assertEqual(len(captured.records), 8)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "\n\n### Non-recursive file_traverser, skipping directory tests/test_data/test-recursive-traverser/6-test-empty-dir")
        self.assertEqual(captured.records[2].getMessage(), "\n\nCalling XML traversal with filename tests/test_data/test-recursive-traverser/test-treq-file-1.md")
        self.assertEqual(captured.records[3].getMessage(), "   ### Processing elements in File tests/test_data/test-recursive-traverser/test-treq-file-1.md")
        self.assertEqual(captured.records[4].getMessage(), "\n\nCalling XML traversal with filename tests/test_data/test-recursive-traverser/test-treq-file-2.md")
        self.assertEqual(captured.records[5].getMessage(), "   ### Processing elements in File tests/test_data/test-recursive-traverser/test-treq-file-2.md")
        self.assertEqual(captured.records[6].getMessage(), "\n\nCalling XML traversal with filename tests/test_data/test-recursive-traverser/test-treq-file-3.py")
        self.assertEqual(captured.records[7].getMessage(), "   ### Processing elements in File tests/test_data/test-recursive-traverser/test-treq-file-3.py")

    # <treqs-element id="0316421cc79211eba976f018989356c1" type="unittest">
    # Tests that non-XML MD files work, and that nested treqs-elements are found
    # <treqs-link type="tests" target="bc89e02a76c811ebb811cf2f044815f7" />
    # <treqs-link type="tests" target="638fa22e76c911ebb811cf2f044815f7" />
    # </treqs-element>
    def test_non_xml_md_files(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            f_traverse = file_traverser()
            success = f_traverse.traverse_file_hierarchy('./tests/test_data/6-test-traverse-treqs.md', False, self.print_handler_function, f_traverse.traverse_XML_file, ".//treqs-element")

            self.assertEqual(success, 0)

        self.assertEqual(len(captured.records),6)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")

        self.assertEqual(captured.records[1].getMessage(), "\n\nCalling XML traversal with filename ./tests/test_data/6-test-traverse-treqs.md")
        self.assertEqual(captured.records[2].getMessage(), "   ### Processing elements in File ./tests/test_data/6-test-traverse-treqs.md")
        self.assertEqual(captured.records[3].getMessage(), "   | requirement | 0276e84ac79011ebb719f018989356c1 |")
        self.assertEqual(captured.records[4].getMessage(), "   | requirement | ff403b04c78f11ebbdc9f018989356c1 |")
        self.assertEqual(captured.records[5].getMessage(), "   | requirement | c5ae0c10c79211eb9631f018989356c1 |")

    # <treqs-element id="330c1b6cc79311eba583f018989356c1" type="unittest">
    # Tests that non-XML non-MD (py) files work
    # <treqs-link type="tests" target="a0820e06-9614-11ea-bb37-0242ac130002" />
    # </treqs-element>
    def test_non_xml_py_files(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            f_traverse = file_traverser()
            success = f_traverse.traverse_file_hierarchy('./tests/test_data/6-test-traverse-treqs.py', False, self.print_handler_function, f_traverse.traverse_XML_file, ".//treqs-element")

            self.assertEqual(success, 0)

        self.assertEqual(len(captured.records),4)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")

        self.assertEqual(captured.records[1].getMessage(), "\n\nCalling XML traversal with filename ./tests/test_data/6-test-traverse-treqs.py")
        self.assertEqual(captured.records[2].getMessage(), "   ### Processing elements in File ./tests/test_data/6-test-traverse-treqs.py")
        self.assertEqual(captured.records[3].getMessage(), "   | unittest | 9c0adc12c79211ebb9cbf018989356c1 |")

    def test_non_existing_non_recursive_file_traverser(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            f_traverse = file_traverser()
            success = f_traverse.traverse_file_hierarchy('./tests/test_data/6-link', False, self.print_handler_function, f_traverse.traverse_XML_file, ".//treqs-element")

            self.assertEqual(success, 1)

        self.assertEqual(len(captured.records), 2)
        self.assertEqual(captured.records[0].getMessage(), "file_traverser created")
        self.assertEqual(captured.records[1].getMessage(), "\n\n### File or directory ./tests/test_data/6-link does not exist. Skipping.")
    
    def test_treqs_ignore(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            f_traverse = file_traverser()
            success = f_traverse.traverse_file_hierarchy('tests/test-ignored-files/', True, self.print_handler_function, f_traverse.traverse_XML_file, ".//treqs-element")

            self.assertEqual(success, 0)

        self.assertEqual(len(captured.records), 4)
        self.assertEqual(captured.records[0].getMessage(), 'file_traverser created')

        records = [r.getMessage() for r in captured.records[1:4]]
        self.assertIn('   ### Ignoring file tests/test-ignored-files/ignored-file.md (.treqs-ignore)', records)
        self.assertIn('\n\nCalling XML traversal with filename tests/test-ignored-files/non-ignored-file.md', records)
        self.assertIn('   ### Processing elements in File tests/test-ignored-files/non-ignored-file.md', records)

    def test_XML_traversal_without_selector(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            f_traverse = file_traverser()
            success = f_traverse.traverse_file_hierarchy('tests/test_data/6-test-traverse-treqs-without_selector.md', False, self.print_tag_handler_function, f_traverse.traverse_XML_file)

            self.assertEqual(success, 0)

        self.assertEqual(len(captured.records), 5)
        self.assertEqual(captured.records[0].getMessage(), 'file_traverser created')
        self.assertEqual(captured.records[1].getMessage(), '\n\nCalling XML traversal with filename tests/test_data/6-test-traverse-treqs-without_selector.md')
        self.assertEqual(captured.records[2].getMessage(), '   ### Processing elements in File tests/test_data/6-test-traverse-treqs-without_selector.md')
        self.assertEqual(captured.records[3].getMessage(), 'a')
        self.assertEqual(captured.records[4].getMessage(), 'b')

    def test_XML_traversal_with_parser_error(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            f_traverse = file_traverser()
            success = f_traverse.traverse_file_hierarchy('tests/test_data/6-test-traverse-treqs-with_parser_error.md', False, self.print_tag_handler_function, f_traverse.traverse_XML_file)

            self.assertEqual(success, 0)

        self.assertEqual(len(captured.records), 4)
        self.assertEqual(captured.records[0].getMessage(), 'file_traverser created')
        self.assertEqual(captured.records[1].getMessage(), '\n\nCalling XML traversal with filename tests/test_data/6-test-traverse-treqs-with_parser_error.md')
        self.assertEqual(captured.records[2].getMessage(), '   ### Processing elements in File tests/test_data/6-test-traverse-treqs-with_parser_error.md')
        self.assertEqual(captured.records[3].getMessage(), '   ### Skipping elements in File tests/test_data/6-test-traverse-treqs-with_parser_error.md due to parser error (("expected \'>\', line 8, column 1",))')

    def test_XML_traversal_ignore_plantuml_block(self):
        with self.assertLogs('treqs-on-git.treqs-ng', level=10) as captured:
            f_traverse = file_traverser()
            success = f_traverse.traverse_file_hierarchy('tests/test_data/real-world-samples/sensit-requirements-draft.md', False, self.silent_handler_function, f_traverse.traverse_XML_file, ".//treqs-element")

            self.assertEqual(success, 0)

        self.assertEqual(len(captured.records), 7)
        self.assertEqual(captured.records[0].getMessage(), 'file_traverser created')
        self.assertEqual(captured.records[1].getMessage(), '\n\nCalling XML traversal with filename tests/test_data/real-world-samples/sensit-requirements-draft.md')
        self.assertEqual(captured.records[2].getMessage(), '   ### Processing elements in File tests/test_data/real-world-samples/sensit-requirements-draft.md')
        self.assertEqual(captured.records[3].getMessage(), '   Found plantuml tag. Ignoring...')
        self.assertEqual(captured.records[4].getMessage(), '   End of plantuml tag.')
        self.assertEqual(captured.records[5].getMessage(), '   Found plantuml tag. Ignoring...')
        self.assertEqual(captured.records[6].getMessage(), '   End of plantuml tag.')

if __name__ == "__main__":
    unittest.main()